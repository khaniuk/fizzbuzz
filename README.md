# fizzbuzz

Ejercicio: Dado un rango de número (ejemplo 1, 50), imprimir fizz en caso de ser múltiplo de 3, buzz en caso de ser múltiplo de 5 y fizzbuzz si es múltiplo de 3 y 5.

## Install dependencies

```
yarn install
```

## Run script

```
node index
```

## Run test

```
yarn test

yarn test:watch
```
