const fizzbuzz = require("../src/app");

describe("fizzbuzz", () => {
  test("Print 0 if they receive 0", () => {
    const expected = 0;
    const result = fizzbuzz(0);
    expect(expected).toBe(result);
  });

  test("Print fizz if they receive 3", () => {
    const expected = "fizz";
    const result = fizzbuzz(3);
    expect(expected).toBe(result);
  });

  test("Print buzz if they receive 5", () => {
    const expected = "buzz";
    const result = fizzbuzz(5);
    expect(expected).toBe(result);
  });

  test("Print fizzbuzz if they receive 3 and 5", () => {
    const expected = "fizzbuzz";
    const result = fizzbuzz(15);
    expect(expected).toBe(result);
  });
});
