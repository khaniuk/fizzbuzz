const fizzbuzz = (num) => {
  const module = (num, multiple) => num % multiple === 0;

  if (num === 0) {
    return num;
  }

  if (module(num, 3) && module(num, 5)) {
    return "fizzbuzz";
  }

  if (module(num, 3)) {
    return "fizz";
  }

  if (module(num, 5)) {
    return "buzz";
  }

  return num;
};

const main = (num) => {
  for (let index = 0; index <= num; index++) {
    console.log(`Number ${index} : ${fizzbuzz(index)}`);
  }
};

main(20);

module.exports = fizzbuzz;
